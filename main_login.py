import os

import uvicorn

if __name__ == "__main__":
    os.chdir("Backend")
    uvicorn.run("Backend.main:app", host="localhost", port=8082, app_dir=os.getcwd(), reload=True,
                log_level="debug")
