# Login Service

This is a lightweight service for authorization
using a JWT token, providing an authorization page
and verification handlers.

The service is based on the FastAPI framework
and contains a web interface written in Vue3.